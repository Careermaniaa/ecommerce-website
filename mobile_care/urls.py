from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('acc_app.urls')),              # For acc_app
    path('', include('admin_app.urls')),            # For admin_app
    path('', include('merchant_app.urls')),            # For merchant_app
    path('', include('store.urls')),                # For store
    path('accounts/', include('allauth.urls')),     # For django-allauth
    path('', include('django.contrib.auth.urls')),  # For password reset
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
