from django.contrib import admin
from merchant_app.models import Product
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'seller', 'price', 'available']
    list_filter = ['available']
    ordering = ['name']

admin.site.register(Product, ProductAdmin)
